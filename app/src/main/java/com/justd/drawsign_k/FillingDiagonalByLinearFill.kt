package com.justd.drawsign_k

import android.graphics.*
import android.os.Build
import androidx.annotation.RequiresApi
import com.justd.drawsign_k.MainActivity.Companion.gFillingTmpGradient
import java.util.*
import kotlin.collections.ArrayList

class FillingDiagonalByLinearFill @RequiresApi(api = Build.VERSION_CODES.O) constructor(
    gBitmapBmp: Bitmap,
    gTouchXInt: Int,
    gTouchYInt: Int
) {
    private var gLPointLeftArr: ArrayList<Path>?    = null
    private var gLPointRightArr: ArrayList<Path>?   = null
    private var gLPointTopArr: ArrayList<Path>?     = null
    private var gLPointBottomArr: ArrayList<Path>?  = null
    private var gBeforePointFpTmp: FloatPoint?      = null

    public var  gLeftIntTmp: Float
    public var  gRightIntTmp: Float
    public var  gTopIntTmp: Float
    public var  gBottomIntTmp: Float

    /*
    iCanvasCvs          : 메인 캔버스
    iBitmapBmp          : 메인 캔버스의 비트맵
    iCirclePaintPnt     : 메인 페인트
    iColorInt           : 채우는 중 터치한 곳과 다른 색깔을 가진 픽셀을 구분하기 위한 터치한 픽셀의 컬러값
    iFillPaintStroke    : iCirclePaintPnt 굵기
    iFillPaintAlpha     : iCirclePaintPnt 투명도
    iFillPaintGap       : 각 라인의 간격(랜덤 값을 곱하여 사용함)
    iFillPaintRand      : 각 라인을 그리는 와중에 삐뚤삐뚤 정도를 조절하는 값
     */
    @RequiresApi(Build.VERSION_CODES.O)
    fun drawLines(
        iCanvasCvs:         Canvas,
        iBitmapBmp:         Bitmap,
        iCirclePaintPnt:    Paint,
        iColorInt:          Int,
        iFillPaintStroke:   Int = 15,
        iFillPaintAlpha:    Int = 185,
        iFillPaintGap:      Int = 4,
        iFillPaintRand:     Int = 3) {

        gLPointLeftArr = ArrayList()
        gLPointRightArr = ArrayList()
        gLPointTopArr = ArrayList()
        gLPointBottomArr = ArrayList()

        var lDirectionInt: Int;
        if(gFillingTmpGradient < 0)
            lDirectionInt = -1;
        else
            lDirectionInt = 1;

        gBeforePointFpTmp = FloatPoint(0F, 0F)
        for (iIntLp in gStartAfpArr.indices) {
            val lCurPointFp = gStartAfpArr[iIntLp]
            val lCurPathPt = Path()
            lCurPointFp.x -= (Random().nextFloat() * 10 - 5).toInt()
            lCurPointFp.y += (Random().nextFloat() * 10 - 5).toInt()
            var lRandXIntTmp = (Random().nextFloat() * 4 - 2).toInt()
            var lRandYIntTmp = (Random().nextFloat() * 4 - 2).toInt()

            //시작점
            if (lCurPointFp.x - lRandXIntTmp < 0 && lCurPointFp.y + lRandYIntTmp < 0) {
                lCurPointFp.x = 0F
                lCurPointFp.y = 0F
                lCurPathPt.moveTo(lCurPointFp.x, lCurPointFp.y)
            } else if (lCurPointFp.x - lRandXIntTmp < 0) {
                lCurPointFp.x = 0F
                lCurPathPt.moveTo(
                    lCurPointFp.x,
                    lRandYIntTmp.let { lCurPointFp.y += it; lCurPointFp.y })
            } else if (lCurPointFp.y + lRandYIntTmp < 0) {
                lCurPointFp.y = 0F
                lCurPathPt.moveTo(
                    lRandXIntTmp.let { lCurPointFp.x -= it; lCurPointFp.x },
                    lCurPointFp.y
                )
            } else {
                lCurPathPt.moveTo(
                    lRandXIntTmp.let { lCurPointFp.x -= it; lCurPointFp.x },
                    lRandYIntTmp.let { lCurPointFp.y += it; lCurPointFp.y })
            }
            if (Math.abs(getDistanceFlt(lCurPointFp, gBeforePointFpTmp!!)) < (Random().nextFloat() * iFillPaintGap - 3).toInt())
                continue

            //07-29
            val lTmpGradient_b: Float = lCurPointFp.y - gFillingTmpGradient * lCurPointFp.x
            var lTmpBeforePointFP: FloatPoint? = null

            var lXIntTmp = lCurPointFp.x.toInt()
            var lYIntTmp = lCurPointFp.y.toInt()

            //07-29
            if(gFillingTmpGradient == 553795F || gFillingTmpGradient == 553796F) {
                while (lXIntTmp < iBitmapBmp.width && lYIntTmp >= 0) {
                    if (lXIntTmp < 0 || lYIntTmp < 0 || lXIntTmp >= iBitmapBmp.width || lYIntTmp >= iBitmapBmp.height) {
                        if(gFillingTmpGradient == 553795F)
                            lYIntTmp ++
                        else
                            lXIntTmp ++;

                        continue
                    }

                    if (lTmpBeforePointFP != null)
                        if (getDistanceFlt(lTmpBeforePointFP, FloatPoint(lXIntTmp.toFloat(), lYIntTmp.toFloat())) < 10) {
                            if(gFillingTmpGradient == 553795F)
                                lYIntTmp ++
                            else
                                lXIntTmp ++;
                            continue
                        }

                    if ((!gPixelsBolArr[iBitmapBmp.width * lYIntTmp + lXIntTmp]) || (iColorInt != iBitmapBmp.getPixel(
                            lXIntTmp,
                            lYIntTmp
                        ))
                    )
                        break
                    else
                        lCurPathPt.lineTo(lXIntTmp.toFloat(), lYIntTmp.toFloat())

                    lTmpBeforePointFP = FloatPoint(lXIntTmp.toFloat(), lYIntTmp.toFloat())

                    if(gFillingTmpGradient == 553795F)
                        lYIntTmp ++
                    else
                        lXIntTmp ++;
                }
            } else {
                while (lXIntTmp < iBitmapBmp.width && lYIntTmp >= 0) {
                    if (lXIntTmp < 0 || lYIntTmp < 0 || lXIntTmp >= iBitmapBmp.width || lYIntTmp >= iBitmapBmp.height) {
                        lXIntTmp += ((Random().nextFloat() * iFillPaintRand - 1)).toInt()
                        lYIntTmp =
                            (gFillingTmpGradient * lXIntTmp + lTmpGradient_b + (Random().nextFloat() * iFillPaintRand - 1)).toInt()

                        continue
                    }
                    if (lTmpBeforePointFP != null)
                        if (getDistanceFlt(lTmpBeforePointFP, FloatPoint(lXIntTmp.toFloat(), lYIntTmp.toFloat())) < 10) {
                            lXIntTmp += ((Random().nextFloat() * iFillPaintRand - 1)).toInt()
                            lYIntTmp =
                                (gFillingTmpGradient * lXIntTmp + lTmpGradient_b + (Random().nextFloat() * iFillPaintRand - 1)).toInt()

                            continue
                        }

                    if (((!gPixelsBolArr[iBitmapBmp.width * lYIntTmp + lXIntTmp]) || (iColorInt != iBitmapBmp.getPixel(lXIntTmp, lYIntTmp)))) {
                        break
                    } else
                        lCurPathPt.lineTo(lXIntTmp.toFloat(), lYIntTmp.toFloat())

                    lTmpBeforePointFP = FloatPoint(lXIntTmp.toFloat(), lYIntTmp.toFloat())

                    lXIntTmp += ((Random().nextFloat() * iFillPaintRand - 1)).toInt()
                    lYIntTmp =
                        (gFillingTmpGradient * lXIntTmp + lTmpGradient_b + (Random().nextFloat() * iFillPaintRand - 1)).toInt()

                }
            }
            gLPointLeftArr!!.add(lCurPathPt)
            gBeforePointFpTmp = lCurPointFp
        }


        gBeforePointFpTmp = FloatPoint(0F, 0F)
        for (iIntLp in gEndAfpArr.indices) {
            val lCurPointFp = gEndAfpArr[iIntLp]
            val lCurPathPt = Path()
            lCurPointFp.x += (Random().nextFloat() * 10 - 5).toInt()
            lCurPointFp.y -= (Random().nextFloat() * 10 - 5).toInt()
            var lRandXIntTmp = (Random().nextFloat() * 4 - 2).toInt()
            var lRandYIntTmp = (Random().nextFloat() * 4 - 2).toInt()

            //시작점
            if (lCurPointFp.x + lRandXIntTmp < 0 && lCurPointFp.y - lRandYIntTmp < 0) {
                lCurPointFp.x = 0F
                lCurPointFp.y = 0F
                lCurPathPt.moveTo(lCurPointFp.x, lCurPointFp.y)
            } else if (lCurPointFp.x + lRandXIntTmp < 0) {
                lCurPointFp.x = 0F
                lCurPathPt.moveTo(lCurPointFp.x, lRandYIntTmp.let { lCurPointFp.y -= it; lCurPointFp.y })
            } else if (lCurPointFp.y - lRandYIntTmp < 0) {
                lCurPointFp.y = 0F
                lCurPathPt.moveTo(lRandXIntTmp.let { lCurPointFp.x += it; lCurPointFp.x }, lCurPointFp.y)
            } else {
                lCurPathPt.moveTo(
                    lRandXIntTmp.let { lCurPointFp.x += it; lCurPointFp.x },
                    lRandYIntTmp.let { lCurPointFp.y -= it; lCurPointFp.y })
            }
            if (Math.abs(getDistanceFlt(lCurPointFp, gBeforePointFpTmp!!)) < (Random().nextFloat() * iFillPaintGap - 3).toInt())
                continue

            //07-29
            val lTmpGradient_b: Float = lCurPointFp.y - gFillingTmpGradient * lCurPointFp.x
            var lTmpBeforePointFP: FloatPoint? = null

            var lXIntTmp = lCurPointFp.x.toInt()
            var lYIntTmp = lCurPointFp.y.toInt()
            if(gFillingTmpGradient == 553795F || gFillingTmpGradient == 553796F) {
                while (lXIntTmp >= 0 && lYIntTmp < iBitmapBmp.height) {
                    if (lXIntTmp < 0 || lYIntTmp < 0 || lXIntTmp >= iBitmapBmp.width || lYIntTmp >= iBitmapBmp.height) {
                        if(gFillingTmpGradient == 553795F)
                            lYIntTmp ++
                        else
                            lXIntTmp --

                        continue
                    }

                    if (lTmpBeforePointFP != null)
                        if (getDistanceFlt(lTmpBeforePointFP, FloatPoint(lXIntTmp.toFloat(), lYIntTmp.toFloat())) < 10) {
                            if(gFillingTmpGradient == 553795F)
                                lYIntTmp ++
                            else
                                lXIntTmp --
                            continue
                        }

                    if ((!gPixelsBolArr[iBitmapBmp.width * lYIntTmp + lXIntTmp]) || (iColorInt != iBitmapBmp.getPixel(lXIntTmp, lYIntTmp)))
                        break
                    else
                        lCurPathPt.lineTo(lXIntTmp.toFloat(), lYIntTmp.toFloat())

                    lTmpBeforePointFP = FloatPoint(lXIntTmp.toFloat(), lYIntTmp.toFloat())
                    if(gFillingTmpGradient == 553795F)
                        lYIntTmp ++
                    else
                        lXIntTmp --
                }
            } else {
                while (lXIntTmp >= 0 && lYIntTmp < iBitmapBmp.height) {
                    if (lXIntTmp < 0 || lYIntTmp < 0 || lXIntTmp >= iBitmapBmp.width || lYIntTmp >= iBitmapBmp.height) {
                        lXIntTmp -= (-1 * (Random().nextFloat() * iFillPaintRand - 1)).toInt()
                        lYIntTmp = (gFillingTmpGradient * lXIntTmp + lTmpGradient_b + (Random().nextFloat() * iFillPaintRand - 1)).toInt()

                        continue
                    }

                    if (lTmpBeforePointFP != null)
                        if (getDistanceFlt(lTmpBeforePointFP, FloatPoint(lXIntTmp.toFloat(), lYIntTmp.toFloat())) < 10) {
                            lXIntTmp -= (-1 * (Random().nextFloat() * iFillPaintRand - 1)).toInt()
                            lYIntTmp = (gFillingTmpGradient * lXIntTmp + lTmpGradient_b + (Random().nextFloat() * iFillPaintRand - 1)).toInt()

                            continue
                        }

                    if ((!gPixelsBolArr[iBitmapBmp.width * lYIntTmp + lXIntTmp]) || (iColorInt != iBitmapBmp.getPixel(lXIntTmp, lYIntTmp)))
                        break
                    else
                        lCurPathPt.lineTo(lXIntTmp.toFloat(), lYIntTmp.toFloat())

                    lTmpBeforePointFP = FloatPoint(lXIntTmp.toFloat(), lYIntTmp.toFloat())

                    lXIntTmp -= (-1 * (Random().nextFloat() * iFillPaintRand - 1)).toInt()
                    lYIntTmp = (gFillingTmpGradient * lXIntTmp + lTmpGradient_b + (Random().nextFloat() * iFillPaintRand - 1)).toInt()
                }
            }
            gLPointRightArr!!.add(lCurPathPt)
            gBeforePointFpTmp = lCurPointFp
        }

        gBeforePointFpTmp = FloatPoint(0F, 0F)
        for (iIntLp in gTopAfpArr.indices) {
            val lCurPointFp = gTopAfpArr[iIntLp]
            val lCurPathPt = Path()
            lCurPointFp.x += (Random().nextFloat() * 10 - 5).toInt()
            lCurPointFp.y -= (Random().nextFloat() * 10 - 5).toInt()
            var lRandXIntTmp = (Random().nextFloat() * 4 - 2).toInt()
            var lRandYIntTmp = (Random().nextFloat() * 4 - 2).toInt()

            //시작점
            if (lCurPointFp.x + lRandXIntTmp < 0 && lCurPointFp.y - lRandYIntTmp < 0) {
                lCurPointFp.x = 0F
                lCurPointFp.y = 0F
                lCurPathPt.moveTo(lCurPointFp.x, lCurPointFp.y)
            } else if (lCurPointFp.x + lRandXIntTmp < 0) {
                lCurPointFp.x = 0F
                lCurPathPt.moveTo(lCurPointFp.x, lRandYIntTmp.let { lCurPointFp.y -= it; lCurPointFp.y })
            } else if (lCurPointFp.y - lRandYIntTmp < 0) {
                lCurPointFp.y = 0F
                lCurPathPt.moveTo(lRandXIntTmp.let { lCurPointFp.x += it; lCurPointFp.x }, lCurPointFp.y)
            } else {
                lCurPathPt.moveTo(
                    lRandXIntTmp.let { lCurPointFp.x += it; lCurPointFp.x },
                    lRandYIntTmp.let { lCurPointFp.y -= it; lCurPointFp.y })
            }
            if (Math.abs(getDistanceFlt(lCurPointFp, gBeforePointFpTmp!!)) < (Random().nextFloat() * iFillPaintGap - 3).toInt())
                continue
            //07-29
            val lTmpGradient_b: Float = lCurPointFp.y - gFillingTmpGradient * lCurPointFp.x
            var lTmpBeforePointFP: FloatPoint? = null

            var lXIntTmp = lCurPointFp.x.toInt()
            var lYIntTmp = lCurPointFp.y.toInt()

            //07-29
            if(gFillingTmpGradient == 553795F || gFillingTmpGradient == 553796F) {
                while (lXIntTmp >= 0 && lYIntTmp <= iBitmapBmp.height) {
                    if (lXIntTmp < 0 || lYIntTmp < 0 || lXIntTmp >= iBitmapBmp.width || lYIntTmp >= iBitmapBmp.height) {
                        if(gFillingTmpGradient == 553795F)
                            lYIntTmp ++
                        else
                            lXIntTmp --
                        continue
                    }

                    if (lTmpBeforePointFP != null)
                        if (getDistanceFlt(lTmpBeforePointFP, FloatPoint(lXIntTmp.toFloat(), lYIntTmp.toFloat())) < 10) {
                            if(gFillingTmpGradient == 553795F)
                                lYIntTmp ++
                            else
                                lXIntTmp --
                            continue
                        }

                    if ((!gPixelsBolArr[iBitmapBmp.width * lYIntTmp + lXIntTmp]) || (iColorInt != iBitmapBmp.getPixel(lXIntTmp, lYIntTmp)))
                        break
                    else
                        lCurPathPt.lineTo(lXIntTmp.toFloat(), lYIntTmp.toFloat())

                    lTmpBeforePointFP = FloatPoint(lXIntTmp.toFloat(), lYIntTmp.toFloat())

                    if(gFillingTmpGradient == 553795F)
                        lYIntTmp ++
                    else
                        lXIntTmp --
                }
            } else {
                while (lXIntTmp >= 0 && lYIntTmp <= iBitmapBmp.height) {
                    if (lXIntTmp < 0 || lYIntTmp < 0 || lXIntTmp >= iBitmapBmp.width || lYIntTmp >= iBitmapBmp.height) {
                        lXIntTmp += (lDirectionInt * (Random().nextFloat() * iFillPaintRand - 1)).toInt()
                        lYIntTmp = (gFillingTmpGradient * lXIntTmp + lTmpGradient_b + (Random().nextFloat() * iFillPaintRand - 1)).toInt()
                        continue
                    }

                    if (lTmpBeforePointFP != null)
                        if (getDistanceFlt(lTmpBeforePointFP, FloatPoint(lXIntTmp.toFloat(), lYIntTmp.toFloat())) < 10) {
                            lXIntTmp += (lDirectionInt * (Random().nextFloat() * iFillPaintRand - 1)).toInt()
                            lYIntTmp = (gFillingTmpGradient * lXIntTmp + lTmpGradient_b + (Random().nextFloat() * iFillPaintRand - 1)).toInt()
                            continue
                        }

                    if ((!gPixelsBolArr[iBitmapBmp.width * lYIntTmp + lXIntTmp]) || (iColorInt != iBitmapBmp.getPixel(lXIntTmp, lYIntTmp)))
                        break
                    else
                        lCurPathPt.lineTo(lXIntTmp.toFloat(), lYIntTmp.toFloat())

                    lTmpBeforePointFP = FloatPoint(lXIntTmp.toFloat(), lYIntTmp.toFloat())

                    lXIntTmp += (lDirectionInt * (Random().nextFloat() * iFillPaintRand - 1)).toInt()
                    lYIntTmp = (gFillingTmpGradient * lXIntTmp + lTmpGradient_b + (Random().nextFloat() * iFillPaintRand - 1)).toInt()
                }
            }
            gLPointTopArr!!.add(lCurPathPt)
            gBeforePointFpTmp = lCurPointFp
        }

        gBeforePointFpTmp = FloatPoint(0F, 0F)
        for (iIntLp in gBottmAfpArr.indices) {
            var lCurPointFp = gBottmAfpArr[iIntLp]
            var lCurPathPt = Path()
            lCurPointFp.x -= (Random().nextFloat() * 10 - 5).toInt()
            lCurPointFp.y += (Random().nextFloat() * 10 - 5).toInt()
            var lRandXIntTmp = (Random().nextFloat() * 4 - 2).toInt()
            var lRandYIntTmp = (Random().nextFloat() * 4 - 2).toInt()
            if (lCurPointFp.x - lRandXIntTmp < 0 && lCurPointFp.y + lRandYIntTmp < 0) {
                lCurPointFp.x = 0F
                lCurPointFp.y = 0F
                lCurPathPt.moveTo(lCurPointFp.x, lCurPointFp.y)
            } else if (lCurPointFp.y + lRandYIntTmp < 0) {
                lCurPointFp.y = 0F
                lCurPathPt.moveTo(lRandXIntTmp.let { lCurPointFp.x -= it; lCurPointFp.x }, lCurPointFp.y)
            } else if (lCurPointFp.x - lRandXIntTmp < 0) {
                lCurPointFp.x = 0F
                lCurPathPt.moveTo(lCurPointFp.x, lRandYIntTmp.let { lCurPointFp.y += it; lCurPointFp.y })
            } else {
                lCurPathPt.moveTo(lRandXIntTmp.let { lCurPointFp.x -= it; lCurPointFp.x }, lRandYIntTmp.let { lCurPointFp.y += it; lCurPointFp.y })
            }
            if (Math.abs(getDistanceFlt(lCurPointFp, gBeforePointFpTmp!!)) < (Random().nextFloat() * iFillPaintGap - 3).toInt())
                continue

            //07-29
            val lTmpGradient_b: Float = lCurPointFp.y - gFillingTmpGradient * lCurPointFp.x
            var lTmpBeforePointFP: FloatPoint? = null

            var lXIntTmp = lCurPointFp.x.toInt()
            var lYIntTmp = lCurPointFp.y.toInt()

            //07-29
            if(gFillingTmpGradient == 553795F || gFillingTmpGradient == 553796F) {
                while (lXIntTmp <= iBitmapBmp.width && lYIntTmp >= 0) {
                    if (lXIntTmp < 0 || lYIntTmp < 0 || lXIntTmp >= iBitmapBmp.width || lYIntTmp >= iBitmapBmp.height) {
                        if(gFillingTmpGradient == 553795F)
                            lYIntTmp --
                        else
                            lXIntTmp ++
                        continue
                    }

                    if (lTmpBeforePointFP != null)
                        if (getDistanceFlt(lTmpBeforePointFP, FloatPoint(lXIntTmp.toFloat(), lYIntTmp.toFloat())) < 10) {
                            if(gFillingTmpGradient == 553795F)
                                lYIntTmp --
                            else
                                lXIntTmp ++
                            continue
                        }

                    if ((!gPixelsBolArr[iBitmapBmp.width * lYIntTmp + lXIntTmp]) || (iColorInt != iBitmapBmp.getPixel(lXIntTmp, lYIntTmp)))
                        break
                    else
                        lCurPathPt.lineTo(lXIntTmp.toFloat(), lYIntTmp.toFloat())

                    lTmpBeforePointFP = FloatPoint(lXIntTmp.toFloat(), lYIntTmp.toFloat())
                    if(gFillingTmpGradient == 553795F)
                        lYIntTmp --
                    else
                        lXIntTmp ++
                }
            }
            else {
                while (lXIntTmp <= iBitmapBmp.width && lYIntTmp >= 0) {
                    if (lXIntTmp < 0 || lYIntTmp < 0 || lXIntTmp >= iBitmapBmp.width || lYIntTmp >= iBitmapBmp.height) {
                        lXIntTmp += (-1*lDirectionInt * (Random().nextFloat() * iFillPaintGap - 1)).toInt()
                        lYIntTmp = (gFillingTmpGradient * lXIntTmp + lTmpGradient_b + (Random().nextFloat() * iFillPaintGap - 1)).toInt()
                        continue
                    }
                    if (lTmpBeforePointFP != null)
                        if (getDistanceFlt(lTmpBeforePointFP, FloatPoint(lXIntTmp.toFloat(), lYIntTmp.toFloat())) < 10) {
                            lXIntTmp += (-1*lDirectionInt * (Random().nextFloat() * iFillPaintGap - 1)).toInt()
                            lYIntTmp = (gFillingTmpGradient * lXIntTmp + lTmpGradient_b + (Random().nextFloat() * iFillPaintGap - 1)).toInt()
                            continue
                        }

                    if ((!gPixelsBolArr[iBitmapBmp.width * lYIntTmp + lXIntTmp]) || (iColorInt != iBitmapBmp.getPixel(lXIntTmp, lYIntTmp)))
                        break
                    else
                        lCurPathPt.lineTo(lXIntTmp.toFloat(), lYIntTmp.toFloat())

                    lTmpBeforePointFP = FloatPoint(lXIntTmp.toFloat(), lYIntTmp.toFloat())

                    lXIntTmp += (-1*lDirectionInt * (Random().nextFloat() * iFillPaintGap - 1)).toInt()
                    lYIntTmp = (gFillingTmpGradient * lXIntTmp + lTmpGradient_b + (Random().nextFloat() * iFillPaintGap - 1)).toInt()
                }
            }
            gLPointBottomArr!!.add(lCurPathPt)
            gBeforePointFpTmp = lCurPointFp
        }

        ///////////////////////////////////////////////

        for (iIntLp in gLPointLeftArr!!.indices) {
            iCirclePaintPnt.alpha = (Random().nextFloat() * iFillPaintAlpha).toInt()
            iCirclePaintPnt.setStrokeWidth((Random().nextFloat() * iFillPaintStroke))
//
            iCanvasCvs.drawPath(gLPointLeftArr!![iIntLp], iCirclePaintPnt)
        }
        for (iIntLp in gLPointRightArr!!.indices) {
            iCirclePaintPnt.alpha = (Random().nextFloat() * iFillPaintAlpha).toInt()
            iCirclePaintPnt.setStrokeWidth((Random().nextFloat() * iFillPaintStroke).toFloat())

            iCanvasCvs.drawPath(gLPointRightArr!![iIntLp], iCirclePaintPnt)
        }
        for (iIntLp in gLPointTopArr!!.indices) {
            iCirclePaintPnt.alpha = (Random().nextFloat() * iFillPaintAlpha).toInt()
            iCirclePaintPnt.setStrokeWidth((Random().nextFloat() * iFillPaintStroke).toFloat())

            iCanvasCvs.drawPath(gLPointTopArr!![iIntLp], iCirclePaintPnt)
        }
        for (iIntLp in gLPointBottomArr!!.indices) {
            iCirclePaintPnt.alpha = (Random().nextFloat() * iFillPaintAlpha).toInt()
            iCirclePaintPnt.setStrokeWidth((Random().nextFloat() * iFillPaintStroke).toFloat())

            iCanvasCvs.drawPath(gLPointBottomArr!![iIntLp], iCirclePaintPnt)
        }
    }

    fun getDistanceFlt(iFpFirstFp: FloatPoint, iFpSecondFp: FloatPoint): Float {
        return Math.sqrt(Math.pow(Math.abs(iFpFirstFp.x - iFpSecondFp.x).toDouble(), 2.0) + Math.pow(Math.abs(iFpFirstFp.y - iFpSecondFp.y).toDouble(), 2.0)).toFloat()
    }

    fun sortingArrWithASC() {
        Collections.sort(gStartAfpArr, DescendingWithY())
        Collections.sort(gEndAfpArr, DescendingWithY())
        Collections.sort(gTopAfpArr, DescendingWithX())
        Collections.sort(gBottmAfpArr, DescendingWithX())
    }

    internal inner class AscendingWithX : Comparator<FloatPoint> {
        override fun compare(iAFpTmp: FloatPoint, iBFpTmp: FloatPoint): Int {
            return iAFpTmp.x.compareTo(iBFpTmp.x)
        }
    }
    internal inner class AscendingWithY : Comparator<FloatPoint> {
        override fun compare(iAFpTmp: FloatPoint, iBFpTmp: FloatPoint): Int {
            return iAFpTmp.y.compareTo(iBFpTmp.y)
        }
    }
    internal inner class DescendingWithX : Comparator<FloatPoint> {
        override fun compare(iAFpTmp: FloatPoint, iBFpTmp: FloatPoint): Int {
            return iBFpTmp.x.compareTo(iAFpTmp.x)
        }
    }

    internal inner class DescendingWithY : Comparator<FloatPoint> {
        override fun compare(iAFpTmp: FloatPoint, iBFpTmp: FloatPoint): Int {
            return iBFpTmp.y.compareTo(iAFpTmp.y)
        }
    }

    companion object {
        private var gStartAfpArr    = ArrayList<FloatPoint>()
        private var gEndAfpArr      = ArrayList<FloatPoint>()
        private var gTopAfpArr      = ArrayList<FloatPoint>()
        private var gBottmAfpArr    = ArrayList<FloatPoint>()
        //07-29
        public lateinit var gPixelsBolArr: BooleanArray
    }

    init {
        gStartAfpArr = ArrayList()
        gEndAfpArr = ArrayList()
        gTopAfpArr = ArrayList()
        gBottmAfpArr = ArrayList()

        //07-29
        gPixelsBolArr = BooleanArray(gBitmapBmp.width * gBitmapBmp.height)

        val queueLinearFloodFillerRow = QueueLinearFloodFillerRow(
            gBitmapBmp,
            gBitmapBmp.getPixel(gTouchXInt, gTouchYInt),
            gBitmapBmp.getPixel(gTouchXInt, gTouchYInt)
        )
        queueLinearFloodFillerRow.setgToleranceIntArr(50)
        queueLinearFloodFillerRow.floodFillRow(gTouchXInt, gTouchYInt)
        gStartAfpArr = queueLinearFloodFillerRow.valuesArr().get(0)
        gEndAfpArr = queueLinearFloodFillerRow.valuesArr().get(1)

        val queueLinearFloodFillerCol = QueueLinearFloodFillerCol(
            gBitmapBmp,
            gBitmapBmp.getPixel(gTouchXInt, gTouchYInt),
            gBitmapBmp.getPixel(gTouchXInt, gTouchYInt)
        )
        queueLinearFloodFillerCol.setgToleranceIntArr(50)
        queueLinearFloodFillerCol.floodFillCol(gTouchXInt, gTouchYInt)
        gTopAfpArr = queueLinearFloodFillerCol.valuesArr().get(0)
        gBottmAfpArr = queueLinearFloodFillerCol.valuesArr().get(1)

        //데이터 afp들을 오름차순으로 정렬 (gap을 이용해서 솎아내기위함)
        sortingArrWithASC()

        gLeftIntTmp = (queueLinearFloodFillerRow.gLeftIntTmp).toFloat()
        gTopIntTmp = (queueLinearFloodFillerCol.gTopIntTmp).toFloat()
        gRightIntTmp = (queueLinearFloodFillerRow.gRightIntTmp).toFloat()
        gBottomIntTmp = (queueLinearFloodFillerCol.gBottomIntTmp).toFloat()

    }
}
