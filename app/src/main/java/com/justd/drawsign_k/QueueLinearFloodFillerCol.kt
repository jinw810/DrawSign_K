package com.justd.drawsign_k

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.justd.drawsign_k.FillingDiagonalByLinearFill.Companion.gPixelsBolArr
import java.util.*


class QueueLinearFloodFillerCol {
    protected lateinit var  gImageBmp: Bitmap
    protected var           gToleranceIntArr = intArrayOf(0, 0, 0)
    protected var           gWidthInt = 0
    protected var           gHeightInt = 0
    protected var           gPixelsIntArr: IntArray? = null
    protected var           gFillColorInt = 0
    protected var           gStartColorIntArr = intArrayOf(0, 0, 0)
    protected lateinit var  gPixelsIntArrChecked: BooleanArray
    protected var           gRangesQue: Queue<FloodFillRange>? = null
    protected var           gTopAfpFltArr = ArrayList<FloatPoint>()
    protected var           gBottomAfpFltArr = ArrayList<FloatPoint>()

    public var  gTopIntTmp: Float = 10000F
    public var  gBottomIntTmp: Float = 0F

    //Construct using an image and a copy will be made to fill into,
    //Construct with BufferedImage and flood fill will write directly to provided BufferedImage
    constructor(iImgBmpIdx: Bitmap) {
        copyImage(iImgBmpIdx)
    }

    constructor(iImgBmpIdx: Bitmap, iTargetColorIntIdx: Int, iNewColorIntIdx: Int) {
        useImage(iImgBmpIdx)
        gFillColorInt = iNewColorIntIdx
        setTargetColor(iTargetColorIntIdx)
    }

    fun setTargetColor(iTargetColorIntIdx: Int) {
        gStartColorIntArr[0] = Color.red(iTargetColorIntIdx)
        gStartColorIntArr[1] = Color.green(iTargetColorIntIdx)
        gStartColorIntArr[2] = Color.blue(iTargetColorIntIdx)
    }

    fun setgToleranceIntArr(iValueIntIdx: Int) {
        gToleranceIntArr = intArrayOf(iValueIntIdx, iValueIntIdx, iValueIntIdx)
    }

    fun copyImage(iImgBmpIdx: Bitmap) {
        //Copy data from provided Image to a BufferedImage to write flood fill to,
        //use getImage to retrieve
        //cache data in member variables to decrease overhead of property calls
        gWidthInt = iImgBmpIdx.width
        gHeightInt = iImgBmpIdx.height
        gImageBmp = Bitmap.createBitmap(gWidthInt, gHeightInt, Bitmap.Config.ARGB_8888)
        var lCanvasTmp = Canvas(gImageBmp)
        lCanvasTmp.drawBitmap(iImgBmpIdx,0f, 0f, null)
        gPixelsIntArr = IntArray(gWidthInt * gHeightInt)
        gImageBmp.getPixels(gPixelsIntArr, 0, gWidthInt, 1, 1, gWidthInt - 1, gHeightInt - 1)
    }

    fun useImage(iImgBmpIdx: Bitmap) {
        //Use a pre-existing provided BufferedImage and write directly to it
        //cache data in member variables to decrease overhead of property calls
        gWidthInt = iImgBmpIdx.width
        gHeightInt = iImgBmpIdx.height
        gImageBmp = iImgBmpIdx
        gPixelsIntArr = IntArray(gWidthInt * gHeightInt)
        gImageBmp!!.getPixels(gPixelsIntArr, 0, gWidthInt, 1, 1, gWidthInt - 1, gHeightInt - 1)
    }

    protected fun prepare() {
        //Called before starting flood-fill
        gPixelsIntArrChecked = BooleanArray(gPixelsIntArr!!.size)
        gRangesQue = LinkedList()
    }

    //    Fills the specified point on the bitmap with the currently selected fill color.
    //    int x, int y: The starting coords for the fill
    @RequiresApi(api = Build.VERSION_CODES.O)
    fun floodFillCol(iXInt: Int, iYInt: Int) {
        //Setup
        prepare()
        if (gStartColorIntArr[0] == 0) {
//            ***Get starting color.
            val lStartPixelInt = gPixelsIntArr!![gHeightInt * iYInt + iXInt]
            gStartColorIntArr[0] = lStartPixelInt shr 16 and 0xff
            gStartColorIntArr[1] = lStartPixelInt shr 8 and 0xff
            gStartColorIntArr[2] = lStartPixelInt and 0xff
        }

//        ***Do first call to floodfill.
        linearFillingCol(iXInt, iYInt)

//        ***Call floodfill routine while floodfill gRangesQue still exist on the queue
        var lRangeTmp: FloodFillRange
        Log.d("<>range", "range size : " + gRangesQue!!.size)
        while (gRangesQue!!.size > 0) {
//            **Get Next Range Off the Queue
            lRangeTmp = gRangesQue!!.remove()

//            **Check Above and Below Each Pixel in the Floodfill Range
            var lDownpxIntIdx = gWidthInt * lRangeTmp.iStartY + lRangeTmp.iX + 1
            var lUppxIntIdx = gWidthInt * lRangeTmp.iStartY + lRangeTmp.iX - 1
            val lUpXInt = lRangeTmp.iX - 1 //so we can pass the y coord by ref
            val lDownXInt = lRangeTmp.iX + 1
            for (iIntLp in lRangeTmp.iStartY..lRangeTmp.iEndY) {


//                *Start Fill Upwards
                //if we're not above the top of the bitmap and the pixel above this one
                //is within the color tolerance
                if (lRangeTmp.iX > 0 && !gPixelsIntArrChecked[lUppxIntIdx] && isCheckPixel(lUppxIntIdx)) {
                    linearFillingCol(lUpXInt, iIntLp)
                }

//                *Start Fill Downwards
                //if we're not below the bottom of the bitmap and the pixel below
                //this one is within the color tolerance
                if (lRangeTmp.iX < gWidthInt - 1 && !gPixelsIntArrChecked[lDownpxIntIdx] && isCheckPixel(lDownpxIntIdx)) {
                    linearFillingCol(lDownXInt, iIntLp)
                }
                lDownpxIntIdx += gWidthInt
                lUppxIntIdx += gWidthInt
            }
        }
        gImageBmp!!.setPixels(gPixelsIntArr, 0, gWidthInt, 1, 1, gWidthInt - 1, gHeightInt - 1)
    }

    fun valuesArr(): Array<ArrayList<FloatPoint>> {
        return arrayOf<ArrayList<FloatPoint>>(gTopAfpFltArr, gBottomAfpFltArr)
    }

    //    Finds the furthermost left and right boundaries of the fill area
    //    on a given y coordinate, starting from a given x coordinate, filling as it goes.
    //    Adds the resulting horizontal range to the queue of floodfill gRangesQue,
    //    to be processed in the main loop.
    //    int x, int y: The starting coords
    @RequiresApi(api = Build.VERSION_CODES.O)
    protected fun linearFillingCol(iXInt: Int, iYInt: Int) {
//        ***Find Left Edge of Color Area -> Top Edge
        var lLFillLocInt = iYInt //the location to check/fill on the left -> Top
        var lPxIdxInt = gWidthInt * iYInt + iXInt
        while (true) {
//            **fill with the color
            gPixelsIntArr!![lPxIdxInt] = gFillColorInt
            // jinu
            // linedata_floatPointdata saveing


//            **indicate that this pixel has already been checked and filled
            gPixelsIntArrChecked[lPxIdxInt] = true
            //07-29
            gPixelsBolArr[lPxIdxInt] = true

//            **de-increment
            lLFillLocInt-- //de-increment counter
            lPxIdxInt -= gWidthInt //de-increment pixel index

//            **exit loop if we're at edge of bitmap or color area
            if (lLFillLocInt < 0 || gPixelsIntArrChecked[lPxIdxInt] || !isCheckPixel(lPxIdxInt)) {
                break
            }
        }
        lLFillLocInt++

        //        ***Find Right Edge of Color Area
        var lRFillLocInt = iYInt //the location to check/fill on the left
        lPxIdxInt = gWidthInt * iYInt + iXInt
        while (true) {
//            **fill with the color
            gPixelsIntArr!![lPxIdxInt] = gFillColorInt

//            **indicate that this pixel has already been checked and filled
            gPixelsIntArrChecked[lPxIdxInt] = true
            //07-29
            gPixelsBolArr[lPxIdxInt] = true
//            afp.add(new FloatPoint(x, y));

//            **increment
            lRFillLocInt++ //increment counter
            lPxIdxInt += gWidthInt //increment pixel index

//            **exit loop if we're at edge of bitmap or color area
            if (lRFillLocInt >= gHeightInt || gPixelsIntArrChecked[lPxIdxInt] || !isCheckPixel(lPxIdxInt)) {
                break
            }
        }
        lRFillLocInt--

        //add range to queue
        val lRRangeTmp = FloodFillRange(lLFillLocInt, lRFillLocInt, iXInt)
        gRangesQue!!.offer(lRRangeTmp)
    }

    //Sees if a pixel is within the color tolerance range.
    @RequiresApi(api = Build.VERSION_CODES.O)
    protected fun isCheckPixel(px: Int): Boolean {
        val lRedIntTmp = gPixelsIntArr!![px] ushr 16 and 0xff
        val lGreenIntTmp = gPixelsIntArr!![px] ushr 8 and 0xff
        val lBlueIntTmp = gPixelsIntArr!![px] and 0xff
        return  lRedIntTmp >= gStartColorIntArr[0] - gToleranceIntArr[0] &&
                lRedIntTmp <= gStartColorIntArr[0] + gToleranceIntArr[0] &&
                lGreenIntTmp >= gStartColorIntArr[1] - gToleranceIntArr[1] &&
                lGreenIntTmp <= gStartColorIntArr[1] + gToleranceIntArr[1] &&
                lBlueIntTmp >= gStartColorIntArr[2] - gToleranceIntArr[2] &&
                lBlueIntTmp <= gStartColorIntArr[2] + gToleranceIntArr[2]
    }

    //    Represents a linear range to be filled and branched from.
    protected inner class FloodFillRange(var iStartY: Int, var iEndY: Int, var iX: Int) {
        init {
            if(iStartY < gTopIntTmp)
                gTopIntTmp = iStartY.toFloat()
            if(iEndY > gBottomIntTmp)
                gBottomIntTmp = iEndY.toFloat()

            if (gTopAfpFltArr.indexOf(FloatPoint(iX.toFloat(), iStartY.toFloat())) == -1)
                gTopAfpFltArr.add(FloatPoint(iX.toFloat(), iStartY.toFloat()))
            if (gBottomAfpFltArr.indexOf(FloatPoint(iX.toFloat(), iStartY.toFloat())) == -1)
                gBottomAfpFltArr.add(FloatPoint(iX.toFloat(), iEndY.toFloat()))
        }
    }
}
