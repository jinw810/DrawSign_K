package com.justd.drawsign_k

import android.content.Context
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import kotlin.collections.ArrayList
import kotlin.math.pow

class MainActivity : AppCompatActivity() {

    lateinit private var mgv: MyGraphicView //2019-01-25 추가

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)

        title = "DrawSign_K"

        mgv = MyGraphicView(this) //20190125
        setContentView(mgv) //201910125
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)

        menu?.add(0, 1, 2, "선 그리기")
        menu?.add(0, 2, 1, "원 그리기")
        menu?.add(0, 3, 0, "네모 그리기")
        menu?.add(0, 10, 0, "지우기") //지우기 추가 2019-01-25

        menu?.add(0, 11, 0, "채우기") //07-22

        val sMenu = menu?.addSubMenu(0, 9, 4, "색상 변경 ==>")

        sMenu?.add(1, 4, 0, "빨강")
        sMenu?.add(1, 5, 0, "파랑")
        sMenu?.add(1, 6, 0, "초록")
        sMenu?.add(2, 7, 1, "선 굵게")
        sMenu?.add(2, 8, 0, "선 가늘게")

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item?.itemId) {
            1 -> {
                curShapeInt = lineInt
                return true
            }
            2 -> {
                curShapeInt = circleInt
                return true
            }
            3 -> {
                curShapeInt = SQ
                return true
            }
            4 -> {
                colorInt = 1
                eraseBeforColorInt = 1
                return true
            }
            5 -> {
                colorInt = 2
                eraseBeforColorInt = 2
                return true
            }
            6 -> {
                colorInt = 3
                eraseBeforColorInt = 3
                return true
            }
            7 -> {
                sizeInt += 5
                return true
            }
            8 -> {
                sizeInt -= 5
                return true
            }
            10 -> { //2019-01-25 추가작성
                myShapes.clear() //ArrayList에 저장된 값들을 지워주는 코드
                eraseBeforColorInt = colorInt //삭제전 색상 저장
                eraseFlag = true //지우기 누른걸 알림
                colorInt = 4
                mgv.invalidate() //mgv의 onDraw를 호출.
                return true
            }
            11 -> { //07-22
                if(fillingFlag)
                    fillingFlag = false
                else
                    fillingFlag = true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    companion object { //동반 객체라고 함.(자바의 static과 비슷한 역할)
        val lineInt = 1 //선
        val circleInt = 2 //원
        val SQ = 3 //네모
        var curShapeInt = lineInt  //모양
        var colorInt = 1 //색상(빨,파,노)
        var sizeInt = 5 //크기(굵게,가늘게)

        //internal은 자바의 default의 역할
        //생성된 클래스와 패키지에서 사용 가능. 다른 패키지 불가.
        internal var myShapes: MutableList<MyShape> = ArrayList()
        //도형들의 데이터 누적
        internal var eraseBeforColorInt = 1

        internal var eraseFlag = false

        //07-29
        internal var fillingFlag = true;
        internal var gFillingTmpGradient: Float = 0F;
        
        //08-06 time object
        internal var gStartTimeLng: Long = 0;
        internal var gEndTimeLng: Long = 0;
        internal var gCurrentTimeLng: Long = 0;

    }

    private class MyGraphicView(context: Context) : View(context) {
        var startX = -1
        var startY = -1
        var stopX = -1
        var stopY = -1

        //07-22
        private lateinit var extraCanvas: Canvas
        private lateinit var extraBitmap: Bitmap
        private var extraPaint: Paint = Paint().apply {
            color = Color.GREEN
            strokeWidth = 5f
            strokeCap = Paint.Cap.ROUND
            style = Paint.Style.STROKE
        }

        private lateinit var extraCanvas2: Canvas
        private lateinit var extraBitmap2: Bitmap
        private var extraPaint2: Paint = Paint().apply {
            color = Color.RED
            strokeWidth = 5f
            strokeCap = Paint.Cap.ROUND
            style = Paint.Style.STROKE
        }
        private var extraPath2: Path? = null;

        override fun onSizeChanged(width: Int, height: Int, oldWidth: Int, oldHeight: Int) {
            super.onSizeChanged(width, height, oldWidth, oldHeight)

            if (::extraBitmap.isInitialized) extraBitmap.recycle()
            extraBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
            extraCanvas = Canvas(extraBitmap)
            extraCanvas.drawColor(Color.WHITE)

            if (::extraBitmap2.isInitialized) extraBitmap2.recycle()
            extraBitmap2 = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
            extraCanvas2 = Canvas(extraBitmap2)
            extraCanvas2.drawColor(Color.TRANSPARENT)


            val drawable = getDrawable(this.context, R.drawable.bitmap)
            val bitmapDrawable = drawable as BitmapDrawable
            val bitmap = bitmapDrawable.bitmap

            extraPath2 = Path();

            extraCanvas.drawRect(100F, 100F, 300F, 300F, extraPaint2)
            extraCanvas.drawRect(500F, 100F, 700F, 300F, extraPaint2)
            extraCanvas.drawRect(700F, 100F, 900F, 300F, extraPaint2)
            extraCanvas.drawCircle(900F, 300F, 100F, extraPaint2)
            extraCanvas.drawRect(100F, 300F, 500F, 500F, extraPaint2)
            extraCanvas.drawCircle(1200F, 700F, 200F, extraPaint2)

            //꽃무늬
//            extraCanvas.drawBitmap(bitmap, 0F, 0F, Paint());
        }

        @RequiresApi(Build.VERSION_CODES.O)
        override fun onTouchEvent(event: MotionEvent?): Boolean {
            when (event?.action) {//액션은 어떤 터치 동작을 했는지 체크하는 역할
                MotionEvent.ACTION_DOWN -> { //터치 시작(손가락을 가져다 둔 것이 ACTION_DOWN)
                    startX = event.x.toInt()
                    startY = event.y.toInt()
                    
                    if (eraseFlag) { //20190125 만약에 지우기를 했다면
                        eraseFlag = false //지우기 하지 않는다으로 바꾸고
                        colorInt = 4 //펜툴 색상을 White로.
                    } else if(fillingFlag) {

//                        extraPath2 = Path()
//                        extraPath2!!.moveTo(startX.toFloat(), startY.toFloat())


                        gStartTimeLng = SystemClock.elapsedRealtime();

                        extraCanvas2.drawCircle(startX-2.5f, startY-2.5f, 5f, extraPaint2);
                        this.invalidate();
                    } else {
                        colorInt = eraseBeforColorInt
                        //지우기가 아니라면 지우기 전 색상을 가져와서 사용하라.
                    }
                }
                MotionEvent.ACTION_MOVE -> {
                    //화면에서 이동할 때 화면에서 손가락 접촉을 멈추었을 때
                    if(fillingFlag) {
                        //07-22
                        //do nothing
                        gEndTimeLng = SystemClock.elapsedRealtime();

                        if(getDistanceFlt(FloatPoint(startX.toFloat(), startY.toFloat()), FloatPoint(event.x, event.y)) < 20) {

                            Log.d("<>tag", "time : " + (gEndTimeLng - gStartTimeLng) + "")

                            extraCanvas2.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                            extraCanvas2.drawCircle(startX.toFloat(), startY.toFloat(), ((5 + (gEndTimeLng - gStartTimeLng)/100).toFloat()).pow(2), extraPaint2);

                            this.invalidate();
                        } else {
                            gStartTimeLng = SystemClock.elapsedRealtime();

                            //07-29
                            extraCanvas2.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                            extraCanvas2.drawCircle(
                                startX.toFloat(),
                                startY.toFloat(),
                                5f,
                                extraPaint2
                            );
                            extraCanvas2.drawLine(
                                startX.toFloat(),
                                startY.toFloat(),
                                event.x,
                                event.y,
                                extraPaint2
                            );

                            var tmpR: Int = (Math.toDegrees(
                                Math.atan2(
                                    (startX - event.x).toDouble(),
                                    (startY - event.y).toDouble()
                                )
                            )).toInt();
                            tmpR = tmpR / 5 * 5;

                            if ((Math.toDegrees(
                                    Math.atan2(
                                        (startX - event.x).toDouble(),
                                        (startY - event.y).toDouble()
                                    )
                                ) % 5).toInt() > 3
                            )
                                tmpR += 5;

                            Log.d("<>tmpr", "tmpr : " + tmpR);

                            if ((tmpR == 90 || tmpR == -90))
                                gFillingTmpGradient = 553795F;
                            else if (tmpR == 180 || tmpR == -180 || tmpR == 0)
                                gFillingTmpGradient = 553796F;
                            else
                                gFillingTmpGradient =
                                    -1 / ((event.y - startY) / (event.x - startX));

                            this.invalidate() //명령이 완료되었으니 그리기를 호출(onDraw호출하는 것)
//                        extraPath2!!.reset()
//                        extraPath2!!.moveTo(startX.toFloat(), startY.toFloat());
//                        extraPath2!!.lineTo(event.x, event.y);
                       }
                    } else {
                        stopX = event.x.toInt()
                        stopY = event.y.toInt()
                    }
                }
                MotionEvent.ACTION_UP -> {
                    if(fillingFlag) {
                        //07-22
                        //do nothing
                        extraCanvas2.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

                        Log.d("<>tmpr","gFillingTmpGradient : " + gFillingTmpGradient)


                        //07-22
                        var fillingDialgonalByLinearFiller = FillingDiagonalByLinearFill(extraBitmap, startX, startY)
                        if(getDistanceFlt(FloatPoint(startX.toFloat(), startY.toFloat()), FloatPoint(event.x, event.y)) > 20) {
                            extraPaint.setShader(
                                LinearGradient(
                                    startX.toFloat(),
                                    startY.toFloat(),
                                    event.x,
                                    event.y,
                                    Color.YELLOW,
                                    Color.RED,
                                    Shader.TileMode.CLAMP
                                )
                            )
                        } else if(getDistanceFlt(FloatPoint(startX.toFloat(), startY.toFloat()), FloatPoint(event.x, event.y)) < 20) {
                            if((gEndTimeLng - gStartTimeLng) < 380) {
                                extraPaint.setShader(null);
                            } else {
                                extraPaint.setShader(
                                    RadialGradient(
                                        startX.toFloat(),
                                        startY.toFloat(),
                                        ((5 + (gEndTimeLng - gStartTimeLng) / 100).toFloat()).pow(2),
                                        Color.YELLOW,
                                        Color.RED,
                                        Shader.TileMode.CLAMP
                                    )
                                )
                            }
                        } else {
                            gFillingTmpGradient = 5F;
                            extraPaint.setShader(null)
                        }
                        fillingDialgonalByLinearFiller.drawLines(extraCanvas, extraBitmap, extraPaint, extraBitmap.getPixel(startX, startY), 18, 185, 4, 3)

                        this.invalidate() //명령이 완료되었으니 그리기를 호출(onDraw호출하는 것)
                    } else {
                        val shape = MyShape() //도형 데이터 1건을 저장시킬 객체생성.
                        shape.shapeType = curShapeInt
                        shape.startX = startX
                        shape.startY = startY
                        shape.stopX = stopX
                        shape.stopY = stopY
                        shape.color = colorInt
                        shape.size = sizeInt
                        myShapes.add(shape) // ArrayList에 저장. 도형 누적.

                        this.invalidate()
                    }
                }
            }
            return true
        }
        fun getDistanceFlt(iFpFirstFp: FloatPoint, iFpSecondFp: FloatPoint): Float {
            return Math.sqrt(Math.pow(Math.abs(iFpFirstFp.x - iFpSecondFp.x).toDouble(), 2.0) + Math.pow(Math.abs(iFpFirstFp.y - iFpSecondFp.y).toDouble(), 2.0)).toFloat()
        }
        override fun onDraw(canvas: Canvas) {
            super.onDraw(canvas)

            val paint = Paint()
            paint.style = Paint.Style.STROKE

//            canvas.drawColor(Color.WHITE) //캔버스 색상 변경 19 01 25

            ////////////////
            if (eraseFlag) { //지우기 했다면
                colorInt = 4//펜툴을 흰색으로 바꾸고
                eraseFlag = false //플래그는 삭제 이전으로 토글.
            }//2019 01 25


            for (i in myShapes.indices) { //리스트 요소 처리
                val shape2 = myShapes[i]
                //첫 번째 도형을 가져와서 shape2에 담아
                paint.setStrokeWidth(shape2.size.toFloat())
                //각 도형별(첫 번째부터) 사이즈를 가져와서 펜 설정.

                if (shape2.color === 1) {
                    //===이 3개인 것은(=가 3개) 객체 비교시 사용
                    //자바에서는 객체 비교 시 equlas를 썼는데
                    //코틀린에서는 =를 3개 써서 객체 비교(===)
                    paint.color = Color.RED
                } else if (shape2.color === 2) {
                    paint.color = Color.BLUE
                } else if (shape2.color === 3) {
                    paint.color = Color.GREEN
                } else if (shape2.color === 4) {
                    paint.color = Color.WHITE
                }

                when (shape2.shapeType) {
                    lineInt -> canvas.drawLine(
                        shape2.startX.toFloat(),
                        shape2.startY.toFloat(),
                        shape2.stopX.toFloat(),
                        shape2.stopY.toFloat(),
                        paint
                    )

                    circleInt -> {
                        val radius = Math.sqrt(
                            Math.pow(
                                (shape2.stopX - shape2.startX).toDouble(),
                                2.0
                            ) + Math.pow(
                                (shape2.stopY - shape2.startY).toDouble(),
                                2.0
                            )
                        ).toInt()
                        canvas.drawCircle(
                            shape2.startX.toFloat(),
                            shape2.startY.toFloat(), radius.toFloat(), paint
                        )
                    }//

                    SQ -> canvas.drawRect(
                        shape2.startX.toFloat(),
                        shape2.startY.toFloat(), shape2.stopX.toFloat(),
                        shape2.stopY.toFloat(), paint
                    )
                }
            }
            paint.strokeWidth = sizeInt.toFloat()

            if (colorInt === 1) {
                paint.color = Color.RED
            } else if (colorInt === 2) {
                paint.color = Color.BLUE
            } else if (colorInt === 3) {
                paint.color = Color.GREEN
            } else if (colorInt === 4) {
                paint.color = Color.WHITE
            }

            when (curShapeInt) {
                lineInt -> canvas?.drawLine(
                    startX.toFloat(), startY.toFloat(), stopX.toFloat(), stopY.toFloat(), paint
                )

                circleInt -> {
                    val radius = Math.sqrt(
                        Math.pow(
                            (stopX - startX).toDouble(), 2.0
                        ) +
                                Math.pow((stopY - startY).toDouble(), 2.0)
                    )

                    canvas?.drawCircle(
                        startX.toFloat(), startY.toFloat(),
                        radius.toFloat(), paint
                    )
                }
                SQ -> extraCanvas?.drawRect(startX.toFloat(), startY.toFloat(), stopX.toFloat(), stopY.toFloat(), paint)
            }

            canvas.drawBitmap(extraBitmap, 0f, 0f, null)

            canvas.drawBitmap(extraBitmap2, 0f, 0f, null)
        }
    }
}