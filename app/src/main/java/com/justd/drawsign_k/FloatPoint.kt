package com.justd.drawsign_k


class FloatPoint {
    var x: Float
    var y: Float

    constructor(x: Float = 0F, y: Float = 0F) {
        this.x = x
        this.y = y
    }
}
